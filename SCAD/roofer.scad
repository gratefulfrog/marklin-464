$fn=100;

dxfFileName   = "../DXF/roof 03.dxf";
roofLayerName = "0";
singleCLayerName = "singleC";
rRoof         = 25.0;
widthHouse    = 40;
hUnderEaves   = 10;
sinAlpha = widthHouse/(2*rRoof);
angleRotation = 2* asin(sinAlpha) + 2* atan(0.2); //cos(0.6);
epsilonWidth = 5;

chimneyDXFFileName = "../DXF/roofProjection 03.dxf";
chimneyLayerName = "chim";
cutterLayerName = "cutter";

houseDXFFileName = "../DXF/roofProjection 03a.dxf";
houseLayerName = "house";
houseCurveLayerName = "houseCurve";
houseZ            = 10;
houseEndThickness = 0.75;
houseY            = 53;

module roofIt(layerName = roofLayerName){
  rotate_extrude(angle=angleRotation)
    translate([(widthHouse+10)/2.0,0,0])
      import(dxfFileName,layer=layerName);
}

//roofIt();

module centerRoof(){
  rotate([0,0,25.56])
    translate([-4.8028,-10.0421,0])
      roofIt();
}

module projectIt(){
  projection()
  roofIt(singleCLayerName);
}
//projectIt();

module centerProjection(){
  rotate([0,0,25.56])
    translate([-4.8028,-10.0421,0])
      projectIt();
}
//centerProjection();

module chimneyRaw(){
   rotate_extrude(angle=360)
    import(chimneyDXFFileName,layer=chimneyLayerName);
}
//chimneyRaw();

module cutter(){
  cylinder(h=100,r=2.5,center=true);
  rotate([90,0,0])
   linear_extrude(50,center=true)
    import(chimneyDXFFileName,layer=cutterLayerName);
}
//cutter();

module chimney(){
  rotate([180,0,0])
    difference(){
      chimneyRaw();
      cutter();
    }
}
//chimney();

module peg(){
  h = 1.0;
  r = 1.0;
  yAlpha = 42;
  translate([18.76,6.5,0])
    rotate([0,-90,yAlpha])
      cylinder(h,r,r);
}
//projection()
  //peg();

module pegX4(){
  innerH = 51.5;
  roofH = 64; // or 64.4 ??
  pegR = 1;
  deltaZ = (roofH-innerH)/2.0 + pegR;
  //echo (deltaZ);
  zVec = [deltaZ,roofH-deltaZ];
  for (z=zVec){
    translate([0,0,z])
      peg();
    mirror([1,0,0])
      translate([0,0,z])
        peg();  
  }  
}
module roofPegged(){
  centerRoof();
  pegX4();
}
// roofPegged();

module houseBox(){
  linear_extrude(houseZ)
    import(houseDXFFileName,layer=houseLayerName);
}
//houseBox();

module houseCurves(){
  yVec = [houseEndThickness,houseY];
  for (y=yVec)
  translate([0,y,houseZ])
    rotate([90,0,0])
      linear_extrude(houseEndThickness)
        import(houseDXFFileName,layer=houseCurveLayerName);
}
//houseCurves();

module house(){
  houseBox();
  houseCurves();
}
//projection()
//house();